package com.tweebaa.ossview.services

import com.aliyun.oss.HttpMethod
import com.aliyun.oss.OSSClient
import com.aliyun.oss.common.auth.DefaultCredentialProvider
import com.aliyun.oss.model.GeneratePresignedUrlRequest
import com.aliyun.oss.model.ListObjectsV2Request
import com.tweebaa.ossview.configs.ApplicationProperties
import com.tweebaa.ossview.models.FileQueryResult
import org.joda.time.DateTime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class OSSService {

    @Autowired
    private lateinit var appConfig: ApplicationProperties

    private val ossClientMap = mutableMapOf<String, OSSClient>()
    private val ossCfgMap = mutableMapOf<String, ApplicationProperties.OssProperties>()

    @PostConstruct
    fun init() {
        appConfig.aliApis?.forEach { ali ->
            ali.services?.oss?.forEach {
                val client = OSSClient(it.endpoint, DefaultCredentialProvider(ali.key, ali.secret), null)
                ossClientMap[it.key!!] = client
                ossCfgMap[it.key!!] = it
            }
        }
//        ossClient = OSSClient(endpoint, DefaultCredentialProvider(accessKeyId, accessKeySecret), null)
    }

    fun listObjects(bucketKey: String, prefix: String, limit: Int, continuationToken: String?): FileQueryResult {
        val cfg = ossCfgMap[bucketKey]
        val bucket = cfg!!.bucket!!
        val query = ListObjectsV2Request(bucket)
        query.prefix = prefix
        query.maxKeys = limit
        query.delimiter = "/"
        if (null != continuationToken) {
            query.continuationToken = continuationToken
        }
        val ossClient = ossClientMap[bucketKey]!!
        val queryResult = ossClient.listObjectsV2(query)
        val previewUrl = mutableMapOf<String, String>()
        val urls = mutableMapOf<String, String>()
        queryResult.objectSummaries.forEach {
            if (it.key.endsWith(".jpeg") || it.key.endsWith(".jpg") || it.key.endsWith(".png")) {
                val requestPreviewUrl = GeneratePresignedUrlRequest(bucket, it.key, HttpMethod.GET)
                requestPreviewUrl.expiration = DateTime.now().plusDays(1).toDate()
                requestPreviewUrl.process = "image/resize,m_lfit,h_100,w_100"
                previewUrl[it.key] = ossClient.generatePresignedUrl(requestPreviewUrl).toString()

                val requestFileUrl = GeneratePresignedUrlRequest(bucket, it.key, HttpMethod.GET)
                requestFileUrl.expiration = DateTime.now().plusDays(1).toDate()

                urls[it.key] = ossClient.generatePresignedUrl(requestFileUrl).toString()
            }
        }
        return FileQueryResult(queryResult, previewUrl, urls)
    }

    fun delete(bucketKey: String, fileIds: List<String>) {
        val cfg = ossCfgMap[bucketKey]
        val bucket = cfg!!.bucket!!
        val ossClient = ossClientMap[bucketKey]!!
        fileIds.forEach {
            ossClient.deleteObject(bucket, it)
        }
    }
}