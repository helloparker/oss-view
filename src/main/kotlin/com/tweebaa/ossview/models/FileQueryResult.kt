package com.tweebaa.ossview.models

import com.aliyun.oss.model.ListObjectsV2Result

data class FileQueryResult(
    val fileInfos: ListObjectsV2Result,
    val previewUrls: Map<String, String>,
    val urls: Map<String, String>
)
