package com.tweebaa.ossview.controllers

import com.aliyun.oss.model.ListObjectsV2Result
import com.tweebaa.ossview.configs.ApplicationProperties
import com.tweebaa.ossview.models.FileQueryResult
import com.tweebaa.ossview.services.OSSService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/oss")
class OSSController {
    @Autowired
    private lateinit var ossSvc: OSSService

    @GetMapping("files")
    fun query(@RequestParam params: Map<String, String>): FileQueryResult {
        val bucketKey = params["bucketKey"]!!
        val prefix = params["prefix"] ?: ""
        val limit = params["limit"]?.toInt() ?: 100
        val continuationToken = params["continuationToken"]
        return ossSvc.listObjects(bucketKey, prefix, limit, continuationToken)
    }

    @DeleteMapping("files")
    fun deleteFile(@RequestParam("bucketKey") bucketKey: String, @RequestBody fileIds: List<String>) {
        ossSvc.delete(bucketKey, fileIds)
    }

    @Autowired
    private lateinit var appConfig: ApplicationProperties

    @GetMapping("buckets")
    fun buckets(): List<ApplicationProperties.OssProperties> {
        val buckets = mutableListOf<ApplicationProperties.OssProperties>()
        appConfig.aliApis?.forEach { aliApiProperties ->
            aliApiProperties.services?.oss?.forEach {
                buckets.add(it)
            }
        }
        return buckets
    }
}