package com.tweebaa.ossview.configs

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.NoOpPasswordEncoder

@Configuration
class SecurityConfiguration : WebSecurityConfigurerAdapter() {
    @Autowired
    private lateinit var appProperties: ApplicationProperties
    override fun configure(auth: AuthenticationManagerBuilder) {
        val config = auth.inMemoryAuthentication()
            .passwordEncoder(BCryptPasswordEncoder())
        appProperties.users?.forEach {
            val roles = mutableSetOf<String>()
            roles.add("users")
            it.roles?.forEach {
                roles.add(it)
            }
            config.withUser(it.name)
                .password(it.password)
                .roles(*roles.toTypedArray())
        }
    }

    override fun configure(http: HttpSecurity) {
        http
            .cors().disable()
            .csrf().disable()
            .formLogin()
            .and()
            .authorizeRequests()
            .anyRequest().authenticated()
    }

}