package com.tweebaa.ossview.configs

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("oss-view")
class ApplicationProperties {
    var aliApis: List<AliApiProperties>? = null
    var users: List<UserProperties>? = null

    class OssProperties {
        var bucket: String? = null
        var key: String? = null
        var endpoint: String? = null
        var prefix: String? = null
    }

    class ServicesProperties {
        var oss: List<OssProperties>? = null
    }

    class AliApiProperties {
        var key: String? = null
        var secret: String? = null
        var services: ServicesProperties? = null
    }

    class UserProperties {
        var name: String? = null
        var password: String? = null
        var roles: List<String>? = null
    }
}