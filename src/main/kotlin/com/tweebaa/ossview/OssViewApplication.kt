package com.tweebaa.ossview

import com.tweebaa.ossview.configs.ApplicationProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties::class)
class OssViewApplication

fun main(args: Array<String>) {
    runApplication<OssViewApplication>(*args)
}
